<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use ClickHouseDB\Client;

class BaselineController extends Controller
{
    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
        //
    }

    public function baseline4g(Request $request) {
        ini_set('max_execution_time', 86400);
        ini_set('memory_limit', '10240M');
        $config = [
            'host' => '10.54.36.55',
            'port' => '8123',
            'username' => 'default',
            'password' => 'Tools@2019#'
        ];
        $db = new Client($config);
        $db->database('neisa_warehouse');
        $db->setTimeout(86400);
        $db->setTimeout(86400);
        $db->setConnectTimeOut(86400);

        $query = "select REGIONAL,AREA,VENDOR,ENODEB_NAME,CELL_NAME,NEID,SITEID,TAC,ENODEBID,CI,EARNFCN,PID,FREQUENCY,BANDTYPE,BANDWITH,JMLH_ENODEB,
        JMLH_CELL,METRO_E,OWNER_LINK,TIPE_LINK,FAR_END_LINK,TOTAL_BANDWITH,TANGGAL_ONAIR_LEASE_LINE,SITE_SIMPUL,JUMLAH_SITE_UNDER_SIMPUL,
        STATUS_LOKASI,CLUSTER_SALES,TYPE_BTS,STATUS,NEW_EXISTING,ONAIR,DATE_ONAIR,KPI_PASS,DATE_KPI_PASS,REMARK,DEPARTEMENT,TECHNICAL_AREA,
        LONGITUDE,LATITUDE,ALAMAT,KELURAHAN,KECAMATAN,KABUPATEN,PROVINSI,TOWER_PROVIDER,NAMA_TOWER_PROVIDER,STATUS_PLN,VENDOR_FMC,STATUS_BTS
        from master_baseline_onair_4g_monthly where STATUS_BTS != 'DISMANTLE' AND REGIONAL='".$request->input('regional')."'";
        $statement = $db->select($query);
        // dd($statement->rows());
        $datas = ["data"=>$statement->rows()];
        // dd($datas);
        return response($datas);
    }
 
    public function baseline4g_history(Request $request) {
        ini_set('max_execution_time', 86400);
        ini_set('memory_limit', '10240M');
        $config = [
            'host' => '10.54.36.55',
            'port' => '8123',
            'username' => 'default',
            'password' => 'Tools@2019#'
        ];
        $db = new Client($config);
        $db->database('neisa_warehouse');
        $db->setTimeout(86400);
        $db->setTimeout(86400);
        $db->setConnectTimeOut(86400);

        $query = "select REGIONAL,AREA,VENDOR,ENODEB_NAME,CELL_NAME,NEID,SITEID,TAC,ENODEBID,CI,EARNFCN,PID,FREQUENCY,BANDTYPE,BANDWITH,JMLH_ENODEB,
        JMLH_CELL,METRO_E,OWNER_LINK,TIPE_LINK,FAR_END_LINK,TOTAL_BANDWITH,TANGGAL_ONAIR_LEASE_LINE,SITE_SIMPUL,JUMLAH_SITE_UNDER_SIMPUL,
        STATUS_LOKASI,CLUSTER_SALES,TYPE_BTS,STATUS,NEW_EXISTING,ONAIR,DATE_ONAIR,KPI_PASS,DATE_KPI_PASS,REMARK,DEPARTEMENT,TECHNICAL_AREA,
        LONGITUDE,LATITUDE,ALAMAT,KELURAHAN,KECAMATAN,KABUPATEN,PROVINSI,TOWER_PROVIDER,NAMA_TOWER_PROVIDER,STATUS_PLN,VENDOR_FMC,STATUS_BTS
        from master_baseline_onair_4g_monthly where STATUS_BTS != 'DISMANTLE' AND REGIONAL='".$request->input('regional')."' AND toMonth(DATE_ONAIR) = toMonth(toDate(".date('m',strtotime($request->input('date'))).")) AND toYear(DATE_ONAIR) = toYear(NOW())";
        $statement = $db->select($query);
        // dd($statement->rows());
        $datas = ["data"=>$statement->rows()];
        // dd($datas);
        return response($datas);
    }    

    public function baseline3g(Request $request) {
        ini_set('max_execution_time', 86400);
        ini_set('memory_limit', '10240M');
        $config = [
            'host' => '10.54.36.55',
            'port' => '8123',
            'username' => 'default',
            'password' => 'Tools@2019#'
        ];
        $db = new Client($config);
        $db->database('neisa_warehouse');
        $db->setTimeout(86400);
        $db->setTimeout(86400);
        $db->setConnectTimeOut(86400);

        $query = "select REGIONAL,AREA,VENDOR,RNC_NAME,NODEB_NAME,CELL_NAME,NEID,SITEID,HSDPA,LAC,CI,BTSNUMBER,CELLNUMBER,SAC,PScCode,BANDTYPE,FREQUENCY
        ,FREQ,F1_F2_F3,JMLH_RNC,JMLH_NODEB,JMLH_CELL,METRO_E,OWNER_LINK,TIPE_LINK,FAR_END_LINK,TOTAL_BANDWITH,TANGGAL_ONAIR_LEASE_LINE,SITE_SIMPUL,JUMLAH_SITE_UNDER_SIMPUL
        ,STATUS_LOKASI,CLUSTER_SALES,TYPE_BTS,STATUS,NEW_EXISTING,ONAIR,DATE_ONAIR,KPI_PASS,DATE_KPI_PASS,REMARK,DEPARTEMENT,TECHNICAL_AREA,ALAMAT,KELURAHAN
        ,KECAMATAN,KABUPATEN,PROVINSI,TOWER_PROVIDER,NAMA_TOWER_PROVIDER,STATUS_PLN,VENDOR_FMC,STATUS_BTS
        from master_baseline_onair_3g_monthly where STATUS_BTS != 'DISMANTLE' AND REGIONAL='".$request->input('regional')."'";
        $statement = $db->select($query);
        // dd($statement->rows());
        $datas = ["data"=>$statement->rows()];
        // dd($datas);
        return response($datas);
    }

    public function baseline3g_history(Request $request) {
        ini_set('max_execution_time', 86400);
        ini_set('memory_limit', '10240M');
        $config = [
            'host' => '10.54.36.55',
            'port' => '8123',
            'username' => 'default',
            'password' => 'Tools@2019#'
        ];
        $db = new Client($config);
        $db->database('neisa_warehouse');
        $db->setTimeout(86400);
        $db->setTimeout(86400);
        $db->setConnectTimeOut(86400);

        $query = "select REGIONAL,AREA,VENDOR,RNC_NAME,NODEB_NAME,CELL_NAME,NEID,SITEID,HSDPA,LAC,CI,BTSNUMBER,CELLNUMBER,SAC,PScCode,BANDTYPE,FREQUENCY
        ,FREQ,F1_F2_F3,JMLH_RNC,JMLH_NODEB,JMLH_CELL,METRO_E,OWNER_LINK,TIPE_LINK,FAR_END_LINK,TOTAL_BANDWITH,TANGGAL_ONAIR_LEASE_LINE,SITE_SIMPUL,JUMLAH_SITE_UNDER_SIMPUL
        ,STATUS_LOKASI,CLUSTER_SALES,TYPE_BTS,STATUS,NEW_EXISTING,ONAIR,DATE_ONAIR,KPI_PASS,DATE_KPI_PASS,REMARK,DEPARTEMENT,TECHNICAL_AREA,ALAMAT,KELURAHAN
        ,KECAMATAN,KABUPATEN,PROVINSI,TOWER_PROVIDER,NAMA_TOWER_PROVIDER,STATUS_PLN,VENDOR_FMC,STATUS_BTS
        from master_baseline_onair_3g_monthly where STATUS_BTS != 'DISMANTLE' AND REGIONAL='".$request->input('regional')."' AND toMonth(DATE_ONAIR) = toMonth(toDate(".date('m',$request->input('date')).")) AND toYear(DATE_ONAIR) = toYear(NOW())";
        $statement = $db->select($query);
        // dd($statement->rows());
        $datas = ["data"=>$statement->rows()];
        // dd($datas);
        return response($datas);
    }    

    public function baseline2g(Request $request) {
        ini_set('max_execution_time', 86400);
        ini_set('memory_limit', '10240M');
        $config = [
            'host' => '10.54.36.55',
            'port' => '8123',
            'username' => 'default',
            'password' => 'Tools@2019#'
        ];
        $db = new Client($config);
        $db->database('neisa_warehouse');
        $db->setTimeout(86400);
        $db->setTimeout(86400);
        $db->setConnectTimeOut(86400);

        $query = "select REGIONAL,AREA,VENDOR,BSC_NAME,SITE_NAME,CELL_NAME,NEID,SITEID,LAC,CELL_ID,BTSNUMBER,CELLNUMBER,FREQUENCY,BANDTYPE,NCC,BCC,f0
        ,f1,f2,f3,f4,f5 ,f6,f7,f8,f9,f10,f11,Trx_Sec,Trx_Site,JMLH_SITE,JMLH_BTS,JMLH_CELL,JMLH_BSC,METRO_E,OWNER_LINK,TIPE_LINK,FAR_END_LINK,TOTAL_BANDWITH
        ,TANGGAL_ONAIR_LEASE_LINE,SITE_SIMPUL,JUMLAH_SITE_UNDER_SIMPUL,STATUS_LOKASI,CLUSTER_SALES,TYPE_BTS,STATUS,TYPE_FREQ,NEW_EXISTING,ONAIR,DATE_ONAIR
        ,KPI_PASS,DATE_KPI_PASS,REMARK,DEPARTEMENT,TECHNICAL_AREA ,ALAMAT,KELURAHAN,KECAMATAN,KABUPATEN_KOTA,PROVINSI,TOWER_PROVIDER,NAMA_TOWER_PROVIDER
        ,STATUS_PLN,VENDOR_FMC,STATUS_BTS
        from master_baseline_onair_2g_monthly where STATUS_BTS != 'DISMANTLE' AND REGIONAL='".$request->input('regional')."'";
        $statement = $db->select($query);
        // dd($statement->rows());
        $datas = ["data"=>$statement->rows()];
        // dd($datas);
        return response($datas);
    }

    public function baseline2g_history(Request $request) {
        ini_set('max_execution_time', 86400);
        ini_set('memory_limit', '10240M');
        $config = [
            'host' => '10.54.36.55',
            'port' => '8123',
            'username' => 'default',
            'password' => 'Tools@2019#'
        ];
        $db = new Client($config);
        $db->database('neisa_warehouse');
        $db->setTimeout(86400);
        $db->setTimeout(86400);
        $db->setConnectTimeOut(86400);

        $query = "select REGIONAL,AREA,VENDOR,BSC_NAME,SITE_NAME,CELL_NAME,NEID,SITEID,LAC,CELL_ID,BTSNUMBER,CELLNUMBER,FREQUENCY,BANDTYPE,NCC,BCC,f0
        ,f1,f2,f3,f4,f5 ,f6,f7,f8,f9,f10,f11,Trx_Sec,Trx_Site,JMLH_SITE,JMLH_BTS,JMLH_CELL,JMLH_BSC,METRO_E,OWNER_LINK,TIPE_LINK,FAR_END_LINK,TOTAL_BANDWITH
        ,TANGGAL_ONAIR_LEASE_LINE,SITE_SIMPUL,JUMLAH_SITE_UNDER_SIMPUL,STATUS_LOKASI,CLUSTER_SALES,TYPE_BTS,STATUS,TYPE_FREQ,NEW_EXISTING,ONAIR,DATE_ONAIR
        ,KPI_PASS,DATE_KPI_PASS,REMARK,DEPARTEMENT,TECHNICAL_AREA ,ALAMAT,KELURAHAN,KECAMATAN,KABUPATEN_KOTA,PROVINSI,TOWER_PROVIDER,NAMA_TOWER_PROVIDER
        ,STATUS_PLN,VENDOR_FMC,STATUS_BTS
        from master_baseline_onair_2g_monthly where STATUS_BTS != 'DISMANTLE' AND REGIONAL='".$request->input('regional')."' AND toMonth(DATE_ONAIR) = toMonth(toDate(".date('m',$request->input('date')).")) AND toYear(DATE_ONAIR) = toYear(NOW())";
        $statement = $db->select($query);
        // dd($statement->rows());
        $datas = ["data"=>$statement->rows()];
        // dd($datas);
        return response($datas);
    }
}
