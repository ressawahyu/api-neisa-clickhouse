<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

//data baseline
$router->get('baseline4g/index', 'BaselineController@baseline4g');
$router->get('baseline4g_history/index', 'BaselineController@baseline4g_history');
$router->get('baseline3g/index', 'BaselineController@baseline3g');
$router->get('baseline3g_history/index', 'BaselineController@baseline3g_history');
$router->get('baseline2g/index', 'BaselineController@baseline2g');
$router->get('baseline2g_history/index', 'BaselineController@baseline2g_history');
